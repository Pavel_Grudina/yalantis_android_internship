package com.example.dolphinchild.car;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    ScrollView scrollView;
    LinearLayout linearLayout;
    Toolbar toolbar;

    TableRow firstField;
    TableRow secondField;
    TableRow thirdField;
    TableRow fourthField;

    View firstLine;
    View secondLine;
    View thirdLine;
    View fourthLine;
    View fifthLine;

    TextView head;
    TextView status;
    TextView request;
    TextView requestDate;
    TextView joined;
    TextView joinedDate;
    TextView repairTo;
    TextView repairToDate;
    TextView performer;
    TextView performerName;
    TextView textRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scrollmode);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        initViews();

        initRecyclerViews();

    }

    private void initRecyclerViews(){
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.car_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL, false);

        if(recyclerView != null) {

            recyclerView.setLayoutManager(layoutManager);

        ArrayList<ImageUrl> carPhotos = prepareData();
        DataAdapter adapter = new DataAdapter(getApplicationContext(),carPhotos);

        recyclerView.setAdapter(adapter);
        }

    }

    private ArrayList<ImageUrl> prepareData(){

        ArrayList<ImageUrl> preparedCarUrls = new ArrayList<>();

        ImageUrl imageUrl = new ImageUrl();
        imageUrl.setCarImageUrl(getString(R.string.link_car_before));
        imageUrl.setSecondImageUrl(getString(R.string.link_car_after));
        preparedCarUrls.add(imageUrl);

        return preparedCarUrls;
    }

    public void showToast(View view) {
        String toastText = getToastText(view);
        Toast toast = Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private String getToastText(View view) {

            return view.getClass().getSimpleName();
        }

    private void initViews (){

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);


        firstField = (TableRow) findViewById(R.id.firstField);
        secondField = (TableRow) findViewById(R.id.secondField);
        thirdField = (TableRow) findViewById(R.id.thirdField);
        fourthField = (TableRow) findViewById(R.id.fourthField);

        firstLine = findViewById(R.id.firstLine);
        secondLine = findViewById(R.id.secondLine);
        thirdLine = findViewById(R.id.thirdLine);
        fourthLine = findViewById(R.id.fourthLine);
        fifthLine = findViewById(R.id.fifthLine);

        head = (TextView) findViewById(R.id.head);
        status = (TextView) findViewById(R.id.status);
        request = (TextView) findViewById(R.id.request);
        requestDate = (TextView) findViewById(R.id.requestDate);
        joined = (TextView) findViewById(R.id.joined);
        joinedDate = (TextView) findViewById(R.id.joinedDate);
        repairTo = (TextView) findViewById(R.id.repairTo);
        repairToDate = (TextView) findViewById(R.id.repairToDate);
        performer = (TextView) findViewById(R.id.performer);
        performerName = (TextView) findViewById(R.id.performerName);
        textRequest = (TextView) findViewById(R.id.textRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


}
