package com.example.dolphinchild.car;

/**
 * Created by DolphinChild on 29.03.2016.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<ImageUrl> carImage;
    private Context context;

    public DataAdapter(Context context, ArrayList<ImageUrl> carImage) {
        this.context = context;
        this.carImage = carImage;

    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        Picasso.with(context).load(carImage.get(i).getCarImageUrl()).resize(130, 100).into(viewHolder.firstFieldImgCar);
        Picasso.with(context).load(carImage.get(i).getSecondImageUrl()).resize(130, 100).into(viewHolder.secondFieldImgCar);

    }

    @Override
    public int getItemCount() {
        return carImage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView firstFieldImgCar;
        ImageView secondFieldImgCar;
        public ViewHolder(View view) {
            super(view);

            firstFieldImgCar = (ImageView)view.findViewById(R.id.firstImg);
            secondFieldImgCar = (ImageView)view.findViewById(R.id.secondImg);

        }
    }
}