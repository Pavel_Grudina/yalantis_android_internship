package com.example.dolphinchild.car;

/**
 * Created by DolphinChild on 29.03.2016.
 */
public class ImageUrl {

    private String carImageUrl;
    private String secondImageUrl;

    public String getSecondImageUrl() {
        return secondImageUrl;
    }

    public void setSecondImageUrl(String secondImageUrl) {
        this.secondImageUrl = secondImageUrl;
    }

    public String getCarImageUrl() {
        return carImageUrl;
    }

    public void setCarImageUrl(String carImageUrl) {
        this.carImageUrl = carImageUrl;
    }
}
